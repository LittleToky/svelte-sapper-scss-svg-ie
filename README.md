## Что внутри?

1. [**Svelte Sapper c поддержкой IE и EDGE**](https://confluence.atlassian.com/x/4whODQ) Основа

2. [**svelte-preprocess**](https://github.com/sveltejs/svelte-preprocess) co всеми зависимостями. Теперь можно использовать **scss**. (<style lang="scss">)

3. [**rollup-plugin-svg**](https://www.npmjs.com/package/rollup-plugin-svg) позволяет импортировать svg в виде кода


# Get Started!

1. clone
2. npm i
3. npm run dev

Если вы видите радужного Бората, все хорошо!
Если Борат черно-белый, значит не обрабатывается scss

Что-то в IE/Edge может не работать в dev, тогда

npm run build:dev

и/или

npm run build # has shimport hack for easier debugging
node __sapper__/build
